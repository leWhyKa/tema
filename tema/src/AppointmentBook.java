import java.util.ArrayList;
import java.util.Collections;

public class AppointmentBook
{
private ArrayList<Appointment> book;

public AppointmentBook()
{
    book = new ArrayList<Appointment>();
}

public void addAppointment(Appointment appt)
{
    book.add(appt);
    Collections.sort(book, new AppointmentComparator());
}

public String printAppointments(int day)
{
    String list = "";

    for (int i = 0; i < book.size(); i++)
    {
        if (book.get(i).getDay() == day)
        {
            list = list + "Appointment description: " + book.get(i).getDescription() + "\n" + "Date of Appointment: " +
            book.get(i).getDay() + "\n" + "Time: " + book.get(i).getStart() + " - " + book.get(i).getEnd() + "\n" + "\n";
        }
    }

    return list;
}
}
