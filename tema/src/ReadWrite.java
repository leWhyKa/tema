import java.awt.*;
import java.net.*;
import java.io.*;

class ReadWrite extends Frame {
    String path, dir;
    URL classFile;
    String[] data;
  
    public static void main(String args[]) {
        new ReadWrite();
        new AppointmentBook();
      }

    public ReadWrite() {
        Dimension res = getToolkit().getScreenSize();
        setBackground(new Color(38, 104, 165));
        setForeground(new Color(255, 255, 0));
        setResizable(false);
        adaugaMenuBar();
        setTitle("Exemplu");
        resize(400, 400);
        move((int) ((res.width - 400) / 2), (int) ((res.height - 400) / 2));
        show();
    }

    void adaugaMenuBar() {
        MenuBar men = new MenuBar();
        Menu f = new Menu("File");
        f.add("Open");
        f.add("-");
        f.add("Exit");
        men.add(f);
        setMenuBar(men);
    }

    public boolean handleEvent(Event e) {
        if (e.id == Event.WINDOW_DESTROY) {
            System.exit(0);
        } else if (e.id == Event.ACTION_EVENT && e.target instanceof MenuItem) {
            if ("Exit".equals(e.arg)) {
                System.exit(0);
            } else if ("Open".equals(e.arg)) {
                loadFile();
                return true;
            }
        } else return false;
        return super.handleEvent(e);
    }

    void loadFile() {
        try {
            FileDialog fd = new FileDialog(this, "Open File", 0);
            if (dir != null) fd.setDirectory(dir);
            fd.setVisible(true);
            if (fd.getFile() != null) {
                dir = fd.getDirectory();
                String fisier = fd.getFile();
                path = dir + fisier;
                try {
                    classFile = new URL("file:/" + path);
                } catch (MalformedURLException e) {
                }
                LoadStream(classFile);
                try {
                    writeFile(new File("out.htm"));
                } catch (Exception e) {
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    void LoadStream(URL classFile) {
        try {
            InputStream is = classFile.openStream();
            DataInputStream dis = new DataInputStream(new BufferedInputStream(is));
            data = new String[100000];
            int i = 0;
            while ((data[i] = dis.readLine()) != null) i++;
            String[] data1 = new String[i];
            System.arraycopy(data, 0, data1, 0, i);
            data = data1;
            is.close();
        } catch (IOException e) {
        }
    }

    void writeFile(File f) throws Exception {
    	
    	
    	
        if (path != "") {
            if (f.exists()) f.delete();
            FileWriter fw = new FileWriter(f);
            BufferedWriter bw = new BufferedWriter(fw);
            for (int i = 0; i < data.length; i++) bw.write(modifica(data[i]) + "\n");
            bw.flush();
            bw.close();
        }
    }

    String modifica(String s) {
        return s;
       
    }

}