public class Appointment
{
private String desc;
private int day; 
private int start; 
private int end;

public Appointment(String description, int aptDay, int startTime, int endTime)
{
    desc = description;
    day = aptDay;
    start = startTime;
    end = endTime;
}

public String getDescription()
{
    return desc;
}

public int getDay()
{
    return day;
}

public int getStart()
{
    return start;
}

public int getEnd()
{
    return end;
}
}